import express from 'express';
import 'express-async-errors';
import cors from 'cors';
import routes from '../src/routes/index';
import "reflect-metadata"
import createConnection from "./database";
import { error } from './middlewares/errors'

require('dotenv').config();

createConnection()
const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);
app.use(error);


export default app

