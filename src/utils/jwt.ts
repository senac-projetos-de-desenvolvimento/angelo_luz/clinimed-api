import JWT from 'jsonwebtoken'
import Employee from '../models/Employee'

const jwtKey = process.env.JWT_SECRET_KEY

function employeeTokenGenerator(employee: Employee) {
    return JWT.sign(
        {
          id: employee.id,
          email: employee.email,
          role: employee.role
        },
        String(jwtKey),
        {
            expiresIn: '4h'
        }
    )
}

function decode(token: string) {
    return JWT.decode(token, Object(jwtKey))
}

export { employeeTokenGenerator, decode }