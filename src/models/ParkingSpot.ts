import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
} from "typeorm";

import SpotValue from './SpotValue'

@Entity("parkingSpot")
export default class ParkingSpot {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string

    @ManyToOne(() => SpotValue, { nullable: true , eager: true})
    @JoinColumn({ name: 'spot_value_id' })
    spotValueId: SpotValue | number;

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}