import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import Cashier from './Cashier';

@Entity('outputCashier')
export default class OutputCashier {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column("float4")
  value: number;

  @ManyToOne(() => Cashier)
  @JoinColumn({ name: 'cashier_id' })
  cashierId: Cashier | number;

  @CreateDateColumn({ name: 'Created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'Updated_at' })
  updatedAt: Date;
}
