import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity("spotValue")
export default class SpotValue {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;

    @Column("float8", { nullable: true })
    value: number;

    @Column("float8", { nullable: true })
    daily: number;

    @Column("float8", {  name: 'overnight_stay', nullable: true })
    overnightStay: number;

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}