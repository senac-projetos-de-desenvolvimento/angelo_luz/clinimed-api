import {
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import Client from "./Client";
import Vehicle from "./Vehicle";

@Entity("clientHasVehicle")
export default class ClientHasVehicle {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Vehicle, { nullable: true })
    @JoinColumn({ name: 'vehicle_id' })
    vehicleId: Vehicle;

    @ManyToOne(() => Client, { nullable: true })
    @JoinColumn({ name: 'client_id' })
    clientId: Client;

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}