import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import Cashier from './Cashier';
import ParkingSpotHasVehicle from './ParkingSpotHasVehicle';

@Entity('inputCashier')
export default class InputCashier {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column("float4")
  value: number;

  @ManyToOne(() => Cashier)
  @JoinColumn({ name: 'cashier_id' })
  cashierId: Cashier | number;

  @ManyToOne(() => ParkingSpotHasVehicle, { nullable: true })
  @JoinColumn({ name: 'ParkingSpotHasVehicle_id' })
  parkingSpotHasVehicleId: ParkingSpotHasVehicle;

  @CreateDateColumn({ name: 'Created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'Updated_at' })
  updatedAt: Date;
}
