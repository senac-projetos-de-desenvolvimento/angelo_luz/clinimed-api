import AppError from '../errors/Errors';
import { Request, Response, NextFunction } from 'express';

function error(err: Error, resquest: Request, response: Response, _: NextFunction) {
    if (err instanceof AppError) {
        console.log(err);
        return response.status(err.statusCode).json({
            status: 'error',
            message: err.message,
        });
    }

    console.log(err);
    return response.status(500).json({
        status: 'error',
        message: 'Internal server error',
    });
}

export { error };