import { Request, Response } from "express"

import InputCashierService from '../services/InputCashierService'
const inputCashierService = new InputCashierService()

export default class EmployeeController {
    async create(req: Request, res: Response) {
            const inputCashier = await inputCashierService.create(Number(req.params.employeeId), req.body)
            return res.status(201).json(inputCashier)
    }

    async list(req: Request, res: Response) {
            const listAll = await inputCashierService.list()
            return res.status(200).json(listAll)       
    }

    async getInputsByCashierId(req: Request, res: Response) {
        const inputCashierList = await inputCashierService.getInputsByCashierId(Number(req.params.cashierId))
        return res.status(200).json(inputCashierList)       
   }

   async delete(req: Request, res: Response) {
        await inputCashierService.delete(Number(req.params.id))
        return res.status(200).json('Entrada excluida com sucesso!')            
  }
}
