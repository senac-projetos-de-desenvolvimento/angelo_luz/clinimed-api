import { Request, Response } from "express"

import OutputCashierService from '../services/OutputCashierService'
const outputCashierService = new OutputCashierService()

export default class EmployeeController {
    async create(req: Request, res: Response) {
            const outputCashier = await outputCashierService.create(Number(req.params.employeeId), req.body)
            return res.status(201).json(outputCashier)
    }

    async getOutputsByCashierId(req: Request, res: Response) {
        const outputCashierList = await outputCashierService.getOutputsByCashierId(Number(req.params.cashierId))
        return res.status(200).json(outputCashierList)       
   }

   async getInputAndOutputsByCashierId(req: Request, res: Response) {
    const inputAndOutputCashierList = await outputCashierService.getInputAndOutputsByCashierId(Number(req.params.cashierId))
    return res.status(200).json(inputAndOutputCashierList)       
  }

  async getAllInputsAndOutputs(req: Request, res: Response) {
    const inputAndOutputCashierList = await outputCashierService.getAllInputsAndOutputs()
    return res.status(200).json(inputAndOutputCashierList)       
  }
  
   async delete(req: Request, res: Response) {
    await outputCashierService.delete(Number(req.params.id))
    return res.status(200).json('Saída excluida com sucesso!')            
}
}
