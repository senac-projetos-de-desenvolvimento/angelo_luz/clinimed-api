import { Request, Response } from "express"

import AmountSpotService from '../services/AmountSpotService'
const amountSpotService = new AmountSpotService()

export default class AmountSpotController {
    async create(req: Request, res: Response) {
            const amountSpot = await amountSpotService.create(req.body)
            return res.status(201).json(amountSpot)
    }

    async list(req: Request, res: Response) {
            const listAll = await amountSpotService.list()
            return res.status(200).json(listAll)
    }

    async update(req: Request, res: Response) {
            const amountSpotUpdate = await amountSpotService.Update(Number(req.params.id), req.body)
            return res.status(200).json(amountSpotUpdate)
    }
}
