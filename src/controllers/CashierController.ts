import { Request, Response } from "express"

import CashierService from '../services/CashierService'
const cashierService = new CashierService()

export default class CashierController {
    async create(req: Request, res: Response) {
            const cashier = await cashierService.create(req.body)
            return res.status(201).json(cashier)
    }

    async list(req: Request, res: Response) {
            const listAll = await cashierService.list()
            return res.status(200).json(listAll)
    }

    async getCashierByEmployeeId(req: Request, res: Response) {
        const cashierId = await cashierService.getCashierByEmployeeId(Number(req.params.employeeId)) 
        return res.status(200).json(cashierId)
    }

    async getById(req: Request, res: Response) {
            const cashier = await cashierService.getById(Number(req.params.id))
            return res.status(200).json(cashier)
    }

    async getCashierFormattedByEmployeeId(req: Request, res: Response) {
        const cashiers = await cashierService.getCashierFormattedByEmployeeId(Number(req.params.employeeId)) 
        return res.status(200).json(cashiers)
    }

    async charts(req: Request, res: Response) {
        const charts = await cashierService.charts()
        return res.status(200).json(charts)
}

    async getAllCashierByEmployeeId(req: Request, res: Response) { 
        const cashiers = await cashierService.getAllCashierByEmployeeId(Number(req.params.employeeId))
        return res.status(200).json(cashiers)
  }

    async closeCashier(req: Request, res: Response){
            const closeCashier = await cashierService.closeCashier(Number(req.params.employeeId), req.body)
            return res.status(200).json(closeCashier)       
    }

    async delete(req: Request, res: Response) {
            await cashierService.delete(Number(req.params.id))
            return res.status(200).json('Caixa excluido com sucesso!')
    }

    async update(req: Request, res: Response) {
            const cashierUpdate = await cashierService.Update(Number(req.params.id), req.body)
            return res.status(200).json(cashierUpdate)
    }
}
