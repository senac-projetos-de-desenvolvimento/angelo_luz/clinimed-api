import { Router } from 'express'
import InputCashierController from '../controllers/InputCashierController'
import { authorization } from '../middlewares/authorization'

const inputCashierRouter = Router()
const inputCashierController = new InputCashierController()


inputCashierRouter.get("/getInputByCashierId/:cashierId", inputCashierController.getInputsByCashierId)

inputCashierRouter.use(authorization)

inputCashierRouter.post("/:employeeId", inputCashierController.create)
 
inputCashierRouter.get("/", inputCashierController.list)

inputCashierRouter.delete("/:id", inputCashierController.delete)

export default inputCashierRouter