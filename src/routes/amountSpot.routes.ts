import { Router } from 'express'
import AmountSpotController from '../controllers/AmountSpotController'

const amountSpotRouter = Router()
const amountSpotController = new AmountSpotController()


amountSpotRouter.post("/", amountSpotController.create)
 
amountSpotRouter.get("/", amountSpotController.list)

amountSpotRouter.put("/:id", amountSpotController.update)


export default amountSpotRouter