import { Router } from 'express'
import CashierController from '../controllers/CashierController'
import { authorization } from '../middlewares/authorization'

const cashierRouter = Router()
const cashierController = new CashierController()

cashierRouter.post("/", cashierController.create)

cashierRouter.put("/closeCashier/:employeeId", authorization, cashierController.closeCashier)

cashierRouter.get("/getCashierByEmployeeId/:employeeId", cashierController.getCashierByEmployeeId)

cashierRouter.get("/getCashierFormattedByEmployeeId/:employeeId", cashierController.getCashierFormattedByEmployeeId)

cashierRouter.get("/charts", cashierController.charts)

cashierRouter.get("/getAllcashierByEmployeeId/:employeeId", cashierController.getAllCashierByEmployeeId)

cashierRouter.get("/", cashierController.list)

cashierRouter.get("/:id", cashierController.getById)

cashierRouter.delete("/:id", cashierController.delete)

cashierRouter.put("/:id", cashierController.update)


export default cashierRouter