import { Router } from 'express'
import ParkingSpotHasVehicleController from '../controllers/ParkingSpotHasVehicleController'
import { authorization } from '../middlewares/authorization'

const ParkingSpotHasVehicleRouter = Router()
const parkingSpotHasVehicleController = new ParkingSpotHasVehicleController()


ParkingSpotHasVehicleRouter.post("/", parkingSpotHasVehicleController.create)
 
ParkingSpotHasVehicleRouter.get("/", parkingSpotHasVehicleController.list)

ParkingSpotHasVehicleRouter.get("/listAll", parkingSpotHasVehicleController.listAll)

ParkingSpotHasVehicleRouter.get("/:id", parkingSpotHasVehicleController.getById)

ParkingSpotHasVehicleRouter.get("/amountToPay/:id", parkingSpotHasVehicleController.amountToPay)

ParkingSpotHasVehicleRouter.delete("/:id", parkingSpotHasVehicleController.delete)

ParkingSpotHasVehicleRouter.put("/:id", parkingSpotHasVehicleController.update)

ParkingSpotHasVehicleRouter.put("/checkout/:id/:employeeId", authorization, parkingSpotHasVehicleController.leave)


export default ParkingSpotHasVehicleRouter;