import { Router } from 'express'
import ParkingSPotController from '../controllers/ParkingSpotController'

const parkingSpotRouter = Router()
const parkingSpotController = new ParkingSPotController()


parkingSpotRouter.post("/", parkingSpotController.create)
 
parkingSpotRouter.get("/", parkingSpotController.list)

parkingSpotRouter.get("/:id", parkingSpotController.getById)

parkingSpotRouter.delete("/:id", parkingSpotController.delete)

parkingSpotRouter.put("/:id", parkingSpotController.update)


export default parkingSpotRouter