import { CashierRepository } from '../repositories/CashierRepository'
import { getCustomRepository } from "typeorm"
import { Between } from 'typeorm';
import Employee from '../models/Employee'


import AppError from '../errors/Errors'

export interface Request {
    startingAmount?: number
    endingAmount?: number
    startDate?: Date
    endDate?: Date
    countAmount?: number
    employeeId?: Employee
}

export interface RequestFinalValue {
    finalRealValue: number
}

export interface RequestFinalValueFormatted {
    endingAmount: number,
    CashierDate: string
}

export default class CashierService {

    async create({ startingAmount, endingAmount, endDate, countAmount, employeeId }: Request) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const currentcashierId =  await this.getCashierByEmployeeId(Number(employeeId))

        if(currentcashierId.length > 0){ throw new AppError("Você possui um caixa aberto, feche primeiro para depois criar outro!")}

        const cashier = cashierRepository.create({
            startingAmount,
            endingAmount,
            startDate: new Date(),
            endDate,
            countAmount,
            employeeId
        })

        await cashierRepository.save(cashier)
        return cashier
    }

    async list() {
        const cashierRepository = getCustomRepository(CashierRepository)

        const all = await cashierRepository.find({ relations: ["employeeId"] })

        return all
    }

    async getCashierByEmployeeId(employeeId: number){
        const cashierRepository = getCustomRepository(CashierRepository)
        const cashiers = cashierRepository.find({
            where: { employeeId },
        })
        if (!cashiers){ throw new AppError("Não existe caixa para esse funcionário!")}

        const getCurrentCashier = (await cashiers).filter(cashier => { return cashier.endDate === null })

        const cashierId = getCurrentCashier.map(cashier => { return cashier.id })

       return cashierId
    }

    async getCashierFormattedByEmployeeId(employeeId: number){
        const cashierRepository = getCustomRepository(CashierRepository)

        let cashierDate: Date[]
        let endindAmountUser: number[]
        let array = []

        const cashiers =  await cashierRepository.find({
            where: { employeeId },
        })
        if (!cashiers){ throw new AppError("Não existe caixa para esse funcionário!")}

        for (let count = 0; count < cashiers.length; count++) {
         cashierDate = cashiers.map(cashier => cashier.startDate)
         endindAmountUser = cashiers.map(cashier => cashier.endingAmountUser)
         if(endindAmountUser[count] === null){
            endindAmountUser[count] = 0
         }
         
         array.push([cashierDate[count].toISOString().split("T")[0], endindAmountUser[count]])
    }

       return array

    }

    async getAllCashierByEmployeeId(employeeId: number){
        const cashierRepository = getCustomRepository(CashierRepository)
        const cashiers = cashierRepository.find({
            where: { employeeId },
        })
        if (!cashiers){ throw new AppError("Não existe caixa para esse funcionário!")}

       return cashiers
    }

    async closeCashier(employeeId: number, { finalRealValue }: RequestFinalValue) {
        
        const cashierRepository = getCustomRepository(CashierRepository)

        const currentcashierId = await this.getCashierByEmployeeId(employeeId)
    
        if(!currentcashierId.length) { throw new AppError("Você não tem nenhum caixa aberto!")}
        if(!finalRealValue) { throw new AppError("Informe o valor final!")}

        const cashier = await this.getById(currentcashierId[0]);

        const endingAmount = cashier.startingAmount + cashier.countAmount

        const finalValueComparison = finalRealValue - endingAmount 

        await cashierRepository.update(currentcashierId[0], {
            endDate: new Date(),
            endingAmount: finalValueComparison,
            endingAmountUser: finalRealValue
        })

        const cashierUpdate = await cashierRepository.findOne(currentcashierId[0])

        return { cashierUpdate, finalValueComparison }
    }

    async getById(id: number) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) throw new AppError('Caixa não encontrado!');

        return cashier
    }

    async delete(id: number) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) {
            throw new AppError('Caixa não encontrado!')
        }

        const deleted = await cashierRepository.delete(id)

        return deleted
    }

    async charts(){
        
        const initialDate = new Date("2022-07-16T17:50:59.043Z")
        const finalDate = new Date()
        const cashierRepository = getCustomRepository(CashierRepository)
    //     const result = await cashierRepository.find({
    //         where: {
    //             startDate: Between(
    //                 initialDate,
    //                 finalDate
    //             ),
    //     },
    // }
    // )
    const result = await cashierRepository.find({
        where: {
            employeeId: 2,
            startDate: Between(initialDate, finalDate),
        }})
        
        console.log(result)
    }

    async Update(id: number, { startingAmount, endingAmount, startDate, endDate, countAmount }: Request) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) {
            throw new AppError('Caixa não encontrado!')
        }

        await cashierRepository.update(id, {
            startingAmount,
            endingAmount,
            startDate,
            endDate,
            countAmount
        })

        const cashierUpdate = await cashierRepository.findOne(id)

        return cashierUpdate
    }

    async upadteCountAmount(id: number, countAmount: number){
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) {
            throw new AppError('Caixa não encontrado!')
        }

        await cashierRepository.update(id, {
            countAmount
        })

        const cashierAmountUpdate = await cashierRepository.findOne(id)

        return cashierAmountUpdate
    }

}

