import { UserConfigRepository } from '../repositories/UserConfigRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'

export interface Request {
    employeeConfigId: number
    report: boolean
    register: boolean
    vehicleUpdate: boolean
}

export default class UserConfigService {

    async create({ employeeConfigId, report, register, vehicleUpdate }: Request) {
        const userConfigRepository = getCustomRepository(UserConfigRepository)

        const userConfig = userConfigRepository.create({
            employeeConfigId,
            report,
            register,
            vehicleUpdate
        })

        await userConfigRepository.save(userConfig)
        return userConfig
    }

    async list() {
        const userConfigRepository = getCustomRepository(UserConfigRepository)

        const all = await userConfigRepository.find()

        return all
    }

    async getById(id: number) {
        const userConfigRepository = getCustomRepository(UserConfigRepository)

        const userConfig = await userConfigRepository.findOne(id)

        return userConfig
    }

    async delete(id: number) {
        const userConfigRepository = getCustomRepository(UserConfigRepository)

        const userConfig = await userConfigRepository.findOne(id)

        if (!userConfig) {
            throw new AppError('Cliente não encontrado!')
        }

        const deleted = await userConfigRepository.delete(id)

        return deleted
    }

    async Update(id: number, { report, register, vehicleUpdate }: Request) {
        const userConfigRepository = getCustomRepository(UserConfigRepository)

        const userConfig = await userConfigRepository.findOne(id)

        if (!userConfig) {
            throw new AppError('Cliente não encontrado!')
        }

        await userConfigRepository.update(id, {
            report, register, vehicleUpdate
        })

        const userConfigUpdate = await userConfigRepository.findOne(id)

        return userConfigUpdate
    }

}

