import { SpotValueRepository } from '../repositories/SpotValueRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'

export interface Request {
    type: string
    value: number
    daily: number
    overnightStay: number
}

export default class SpotValueService {

    async create({ type, value, daily, overnightStay }: Request) {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const spotValue = spotValueRepository.create({
            type, value, daily, overnightStay
        })

        await spotValueRepository.save(spotValue)
        return spotValue
    }

    async list() {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const all = await spotValueRepository.find()

        return all
    }

    async getById(id: number) {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const spotValue = await spotValueRepository.findOne(id)

        if (!spotValue) {
            throw new AppError('Vaga não encontrada!')
        }

        return spotValue
    }

    public async getByType(type: string) {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const spotValue = await spotValueRepository.findOne({ type: type })

        if (!spotValue) {
            throw new AppError('Vaga não encontrada!')
        }

        return spotValue.id
    }

    async delete(id: number) {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const spotValue = await spotValueRepository.findOne(id)

        if (!spotValue) {
            throw new AppError('Vaga não encontrada!')
        }

        const deleted = await spotValueRepository.delete(id)

        return deleted
    }

    async Update(id: number, { type, value }: Request) {
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const spotValue = await spotValueRepository.findOne(id)

        if (!spotValue) {
            throw new AppError('Vaga não encontrada!')
        }

        await spotValueRepository.update(id, {
            type,
            value
        })

        const spotValueUpdate = await spotValueRepository.findOne(id)

        return spotValueUpdate
    }

}

