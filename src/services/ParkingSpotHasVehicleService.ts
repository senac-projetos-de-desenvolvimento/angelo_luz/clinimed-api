import { ParkingSpotHasVehicleRepository } from '../repositories/ParkingSpotHasVehicleRepository';
import { ParkingSpotRepository } from '../repositories/ParkingSpotRepository';
import { SpotValueRepository } from '../repositories/SpotValueRepository';
import { getCustomRepository } from "typeorm";

import InputCashierService from './InputCashierService';
const inputCashierService = new InputCashierService()
import CashierService from './CashierService';
const cashierService = new CashierService()

import AppError from '../errors/Errors'
import ParkingSpot from '../models/ParkingSpot'
import Vehicle from '../models/Vehicle'
import Cashier from '../models/Cashier'
import timeCalc from '../utils/timeCalc'

export interface Request {
    parkingSpotId: ParkingSpot,
    vehicleId: Vehicle,
    checkOut: Date | null,
    checkIn: Date,
    totalValue: number | null,
    cashierId?: Cashier
    status: boolean,
}

export default class ParkingSpotHasVehicleService {

    async create({ parkingSpotId, vehicleId, status }: Request) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const spotValue = await parkingSpotRepository.findOne(parkingSpotId)

        const parkingSpotValue = spotValue ? spotValue.spotValueId : 0

        if (parkingSpotValue === 0) {
            throw new AppError('Id não encontrado')
        }

        const parkingSpotHasVehicleCreate = parkingSpotHasVehicleRepository.create({
            parkingSpotId,
            vehicleId,
            checkIn: new Date(),
            status          
        })

        await parkingSpotHasVehicleRepository.save(parkingSpotHasVehicleCreate)
        return parkingSpotHasVehicleCreate
    }

    async listAll() {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const all = await parkingSpotHasVehicleRepository.find({
            relations: ["parkingSpotId", "vehicleId"],
            order: { id: "ASC" },
        })

        return all
    }

    async list() {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const all = await parkingSpotHasVehicleRepository.find({
            relations: ["parkingSpotId", "vehicleId"],
            order: { id: "ASC" },
        })

        const filter = all.filter(items => {
            return items.checkOut === null
        })

        return filter
    }

    async getById(id: number) {

        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const findeOne = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "vehicleId"] })

        if (!findeOne) {
            throw new AppError('Id não encontrado')
        }

        return findeOne
    }

    async amountToPay(id: number) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const spotValueRepository = getCustomRepository(SpotValueRepository)

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "vehicleId"] })

        if (!parkingSpotHasVehicle) {
            throw new AppError('Veículo ou vaga não encontrado!')
        }

        const checkIn = parkingSpotHasVehicle.checkIn
        const spotValueId = parkingSpotHasVehicle.parkingSpotId.spotValueId

        const obj = Object.assign(Object(spotValueId))

        const timeValue = await spotValueRepository.findOne(obj.id)
      
        const checkOut = new Date()

        const data = timeCalc(checkIn, checkOut, timeValue?.value, timeValue.daily, timeValue.overnightStay)

        return data
    }

    async Update(id: number, { parkingSpotId, vehicleId, checkIn, status }: Request) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const findParkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "vehicleId"] })

        if (!findParkingSpotHasVehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        await parkingSpotHasVehicleRepository.update(id, {
            parkingSpotId
        })

        if(findParkingSpotHasVehicle.parkingSpotId.name.includes("Generic")){
            await parkingSpotRepository.delete(findParkingSpotHasVehicle.parkingSpotId.id)
        }

        const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleRepository.findOne(id)

        return parkingSpotHasVehicleUpdate
    }

    async delete(id: number) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id)

        if (!parkingSpotHasVehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const deleted = await parkingSpotHasVehicleRepository.delete(id)

        return deleted
    }

    async leave(id: number, employeeId: number){
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const spotValueRepository = getCustomRepository(SpotValueRepository)
      
       const currentcashierId = await cashierService.getCashierByEmployeeId(employeeId)

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "vehicleId"] })

        if (!parkingSpotHasVehicle) {
            throw new AppError('Veículo ou vaga não encontrado!')
        }
        if (parkingSpotHasVehicle.status === false) throw new AppError('Veículo ja foi dado baixa!')

        const cashiers =  await cashierService.getCashierByEmployeeId(employeeId)

        if (!cashiers.length){ throw new AppError("Não existe caixa para esse funcionário!")}

        const checkIn = parkingSpotHasVehicle.checkIn
        const spotValueId = parkingSpotHasVehicle.parkingSpotId.spotValueId

        const obj = Object.assign(Object(spotValueId))

        const timeValue = await spotValueRepository.findOne(obj.id)
      
        const checkOut = new Date()

        const data = timeCalc(checkIn, checkOut, timeValue?.value, timeValue.daily)

        const { totalValue } = data

        const cashier = await cashierService.getById(currentcashierId[0])

        parkingSpotHasVehicleRepository.update(id, {
            checkOut,
            totalValue,
            cashierId: cashier,
            status: false
        })

       const createInputCashier = Object.assign({
            description: `Saída do veículo de placa: ${parkingSpotHasVehicle.vehicleId.license}`,
            currentcashierId: cashier,
            value: totalValue,
            parkingSpotHasVehicleId: id
        })

        await inputCashierService.create(employeeId, createInputCashier)

        inputCashierService.cashierCountAmountSum(Number(currentcashierId), totalValue)

        return data
    }
}


