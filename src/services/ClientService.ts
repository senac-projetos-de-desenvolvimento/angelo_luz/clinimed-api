import { ClientRepository } from '../repositories/ClientRepository'
import { getCustomRepository, Like } from "typeorm"

import AppError from '../errors/Errors'

interface Request {
    name: string
    phone: string
    email: string
    address: string
    birth: Date
    cpf_cnpj: string
    pay_day: Date
    status: boolean
}

export default class ClientService {

    async create({ name, phone, email, address, birth, cpf_cnpj, pay_day, status }: Request) {
        const clientRepository = getCustomRepository(ClientRepository)

        const clientAlreadyExists = await clientRepository.findOne({
            email
        })

        if (clientAlreadyExists) {
            throw new AppError('Cliente já existente')
        }

        const client = clientRepository.create({
            name: name.toUpperCase(),
            phone,
            email,
            address,
            birth,
            cpf_cnpj,
            pay_day,
            status,
        })

        await clientRepository.save(client)
        return client
    }

    async list() {
        const clientRepository = getCustomRepository(ClientRepository)

        const all = await clientRepository.find()

        return all
    }

    async getByName(name: string) {
        const clientRepository = getCustomRepository(ClientRepository)
        const client = await clientRepository.find({ name: Like (`%${name.toUpperCase()}%`)})
        if (!client) {
            throw new AppError('Cliente não encontrado!')
        }

        return client
    }

    async getById(id: number) {
        const clientRepository = getCustomRepository(ClientRepository)

        const client = await clientRepository.findOne(id)

        if (!client) {
            throw new AppError('Cliente não encontrado!')
        }

        return client
    }
    async delete(id: number) {
        const clientRepository = getCustomRepository(ClientRepository)

        const client = await clientRepository.findOne(id)

        if (!client) {
            throw new AppError('Cliente não encontrado!')
        }

        const deleted = await clientRepository.delete(id)

        return deleted
    }

    async Update(id: number, { name, phone, email, address, birth, cpf_cnpj, pay_day, status }: Request) {
        const clientRepository = getCustomRepository(ClientRepository)

        const client = await clientRepository.findOne(id)

        if (!client) {
            throw new AppError('Cliente não encontrado!')
        }

        const emailAreadyExists = await clientRepository.findOne({
            email
        })

        if (emailAreadyExists) {
            throw new AppError('Erro ao inserir os dados')
        }

        await clientRepository.update(id, {
            name, phone, email, address, birth, cpf_cnpj, pay_day, status
        })

        const clientUpdate = await clientRepository.findOne(id)

        return clientUpdate
    }

}

