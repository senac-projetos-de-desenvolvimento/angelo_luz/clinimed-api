import { InputCashierRepository } from '../repositories/InputCashierRepository';
import { CashierRepository } from '../repositories/CashierRepository';
import { DeleteResult, getCustomRepository } from 'typeorm';
import Cashier from '../models/Cashier';
import InputCashier from '../models/InputCashier';
import ParkingSpotHasVehicle from '../models/ParkingSpotHasVehicle';

import CashierService from './CashierService';
const cashierService = new CashierService();

import AppError from '../errors/Errors';
interface ICreateDTO {
  description: string;
  value: number;
  cashierId: Cashier | number;
  parkingSpotHasVehicleId?: ParkingSpotHasVehicle;
}

interface IMethods {
  create(employeeId: number, { description, cashierId, parkingSpotHasVehicleId}: ICreateDTO): Promise<InputCashier>;
  save(inputCashier: InputCashier): Promise<void>;
  list(): Promise<InputCashier[]>;
  getInputsByCashierId(cashierId: number): Promise<InputCashier[]>;
  cashierCountAmountSum(cashierId: number, value: number): Promise<Boolean>;
  cashierCountAmountsubtraction(cashierId: number, value: number): Promise<Boolean>;
  delete(id: number, employeeId: number): Promise<DeleteResult>;
}

export default class InputCashierService implements IMethods {
  async create(employeeId: number, { description, value, cashierId, parkingSpotHasVehicleId }: ICreateDTO): Promise<InputCashier> {
    const inputCashierRepository = getCustomRepository(InputCashierRepository);
    
    if (!employeeId) throw new AppError('Informe o funcionário!');
    if (!description ) throw new AppError('Informe uma descrição!');

    const currentcashierId = await cashierService.getCashierByEmployeeId(employeeId)

    if(!currentcashierId.length) { throw new AppError("Você não tem nenhum caixa aberto!")}

    const inputCashier = inputCashierRepository.create({
      description,
      value,
      cashierId: currentcashierId[0],
      parkingSpotHasVehicleId,
    });

    this.save(inputCashier);
    this.cashierCountAmountSum(Number(currentcashierId[0]), value)

    return inputCashier;
  }

  async save(inputCashier: InputCashier): Promise<void> {
    const inputCashierRepository = getCustomRepository(InputCashierRepository);
    await inputCashierRepository.save(inputCashier);
  }

  async cashierCountAmountSum(cashierId: number, value: number): Promise<Boolean> {
    const cashierRepository = getCustomRepository(CashierRepository);

    const cashier = await cashierRepository.findOne(Number(cashierId));
    const totalValue = cashier.countAmount += value

    await cashierService.upadteCountAmount(Number(cashierId), totalValue);

    return true
}

  async cashierCountAmountsubtraction(cashierId: number, value: number): Promise<Boolean> {
    const cashierRepository = getCustomRepository(CashierRepository);

    const cashier = await cashierRepository.findOne(Number(cashierId));
    if(cashier.countAmount <= 0 || cashier.countAmount < value) throw new AppError("Caixa não contém valor para ser debitado ou está zerado!");

    const totalValue = cashier.countAmount -= value
 
    await cashierService.upadteCountAmount(Number(cashierId), totalValue);

    return true
}

  async list(): Promise<InputCashier[]> {
    const inputCashierRepository = getCustomRepository(InputCashierRepository);
    const allInputCashier = await inputCashierRepository.find({
      relations: ['cashierId', 'parkingSpotHasVehicleId'],
      order: { id: 'ASC' },
    });
    return allInputCashier;
  }

  async getInputsByCashierId(cashierId: number): Promise<InputCashier[]> {
    const inputCashierRepository = getCustomRepository(InputCashierRepository);
    const allInputCashier = await inputCashierRepository.find({
      where: [{ cashierId }],
      relations: ['cashierId', 'parkingSpotHasVehicleId'],
      order: { id: 'ASC' },
    });

    if (!allInputCashier.length)
      throw new AppError(
        'Não tem entradas para esse caixa, ou caixa inexistente!',
      );
    return allInputCashier;
  }

  async delete(id: number): Promise<DeleteResult> {
    const inputCashierRepository = getCustomRepository(InputCashierRepository)

    const inputCashier = await inputCashierRepository.findOne(id,{
      relations: ['cashierId']
    })

    if (!inputCashier) throw new AppError('Entrada não encontrada!')

    const objInputCashier = Object.assign(Object(inputCashier.cashierId))

    const isTrue = await this.cashierCountAmountsubtraction(objInputCashier.id, inputCashier.value)
    if(!isTrue) throw new AppError('Algo deu errado, não foi debitado o valor do montante do caixa.')

    const deleted = await inputCashierRepository.delete(id)

    return deleted
  }
}
