import { ClientHasVehicleRepository } from '../repositories/ClientHasVehicleRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'
import Vehicle from '../models/Vehicle'
import Client from '../models/Client'

export interface Request {
    vehicleId: Vehicle,
    clientId: Client,
}

export default class CashierService {

    async create({ vehicleId, clientId }: Request) {

        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const clientHasVehicle = clientHasVehicleRepository.create({
            vehicleId,
            clientId
        })

        await clientHasVehicleRepository.save(clientHasVehicle)
        return clientHasVehicle
    }

    async list() {
        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const all = await clientHasVehicleRepository.find({
            relations: ["vehicleId", "clientId"],
            order: { id: "ASC" },
        })

        return all
    }

    async getClientsByVehicleId(vehicleId: number) {
        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const clientHasVehicle = await clientHasVehicleRepository.find({
            where: { vehicleId },
            relations: ["clientId"]
        })

        if (clientHasVehicle.length === 0) {
            throw new AppError('Veículo não encontrado ou não vinculado a um cliente!')
        }

        return clientHasVehicle
    }

    async getVehiclesByClientId(clientId: number) {
        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const clientHasVehicle = await clientHasVehicleRepository.find({
            where: { clientId },
            relations: ["vehicleId"]
        })

        if (clientHasVehicle.length === 0) {
            throw new AppError('Cliente não encontrado ou não vinculado a um veículo!')
        }

        return clientHasVehicle
    }

    async delete(id: number) {
        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const clientHasVehicle = await clientHasVehicleRepository.findOne(id)

        if (!clientHasVehicle) {
            throw new AppError('Vinculação não encontrada!')
        }

        const deleted = await clientHasVehicleRepository.delete(id)

        return deleted
    }

    async Update(id: number, { vehicleId, clientId }: Request) {
        const clientHasVehicleRepository = getCustomRepository(ClientHasVehicleRepository)

        const clientHasVehicle = await clientHasVehicleRepository.findOne(id)

        if (!clientHasVehicle) {
            throw new AppError('Caixa não encontrado!')
        }

        await clientHasVehicleRepository.update(id, {
            vehicleId,
            clientId
        })

        const clientHasVehicleUpdate = await clientHasVehicleRepository.findOne(id)

        return clientHasVehicleUpdate
    }

}

