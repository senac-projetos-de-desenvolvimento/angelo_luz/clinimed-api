import { EntityRepository, Repository } from 'typeorm';
import OutputCashier from '../models/OutputCashier';

@EntityRepository(OutputCashier)
class OutputCashierRepository extends Repository<OutputCashier> {}
export { OutputCashierRepository };
