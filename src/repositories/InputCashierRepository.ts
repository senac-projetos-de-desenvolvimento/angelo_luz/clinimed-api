import { EntityRepository, Repository } from 'typeorm';
import InputCashier from '../models/InputCashier';

@EntityRepository(InputCashier)
class InputCashierRepository extends Repository<InputCashier> {}
export { InputCashierRepository };
