import { EntityRepository, Repository } from "typeorm";
import ClientHasVehicle from "../models/ClientHasVehicle"

@EntityRepository(ClientHasVehicle) 
class ClientHasVehicleRepository extends Repository<ClientHasVehicle> {

}
export{ClientHasVehicleRepository}